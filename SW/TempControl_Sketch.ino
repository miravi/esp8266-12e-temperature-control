/*************************************************************
  Download latest Blynk library here:
    https://github.com/blynkkk/blynk-library/releases/latest

  Blynk is a platform with iOS and Android apps to control
  Arduino, Raspberry Pi and the likes over the Internet.
  You can easily build graphic interfaces for all your
  projects by simply dragging and dropping widgets.

    Downloads, docs, tutorials: http://www.blynk.cc
    Sketch generator:           http://examples.blynk.cc
    Blynk community:            http://community.blynk.cc
    Follow us:                  http://www.fb.com/blynkapp
                                http://twitter.com/blynk_app

  Blynk library is licensed under MIT license
  This example code is in public domain.

 *************************************************************
  This example runs directly on ESP8266 chip.

  Note: This requires ESP8266 support package:
    https://github.com/esp8266/Arduino

  Please be sure to select the right ESP8266 module
  in the Tools -> Board menu!

  Change WiFi ssid, pass, and Blynk auth token to run :)
 *************************************************************/

/* Comment this out to disable prints and save space */
#define BLYNK_PRINT Serial


#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266.h>
#include <WidgetRTC.h>
#include "max6675.h"

#define TRUE    1U
#define FALSE   0U

#define TEMP_DOWN_DIFF_LIMIT                    (0.5f  * 100)
#define TEMP_UP_DIFF_LIMIT                      (0.25f * 100)

#define TEMP_RELAY_CTRL_PIN                     5U  // GPIOx

#define TEMP_LED_ON                             255U
#define TEMP_LED_OFF                            0U

#define TEMP_CONV_MIN_IN_S(X)                   ((uint32_t)(X) * 60U)
#define TEMP_CONV_S_IN_US(X)                    ((uint32_t)(X) * 1000000UL)

#define TEMP_TIMED_ACTIONS_PERIOD_SECONDS       10U

uint8_t ktcSO  = 12U;  // GPIO12
uint8_t ktcCS  = 13U;  // GPIO13
uint8_t ktcCLK = 14U;  // GPIO14

MAX6675 ktc(ktcCLK, ktcCS, ktcSO);

// You should get Auth Token in the Blynk App.
// Go to the Project Settings (nut icon).
char auth[] = "4bc85932d1204150a5c743f3e85728dd";

BlynkTimer timer;

WidgetRTC rtc;
WidgetLED led1(V3); //register to virtual pin 3

WidgetTerminal terminal(V20);

uint8_t debugOn = FALSE;
String currentDate;
String currentTime;

#define TEMP_DEBUG_PRINT(X)         Serial.print((X));   \
                                    terminal.print((X)); \
                                    terminal.flush();

#define TEMP_DEBUG_PRINTLN          Serial.println();   \
                                    terminal.println(); \
                                    terminal.flush();

#define TEMP_DEBUG_PRINT_WITH_TIMESTAMP(X)  currentDate = String(day()) + "." + month() + "." + year();                       \
                                            currentTime = String(hour()) + ":" + minute() + ":" + second();                   \
                                            TEMP_DEBUG_PRINT(String(currentDate) + " " + String(currentTime) + ": " + (X));   \
                                            TEMP_DEBUG_PRINTLN

uint8_t terminalOn = FALSE;

uint8_t heatingOn  = FALSE;

// Your WiFi credentials.
// Set password to "" for open networks.
char ssid[] = "N50";
char pass[] = "7251A592";

uint16_t currentTemperature =  0U;
uint16_t temperatureSetting = 1200U;  // 12.00

uint8_t deepSleepSeconds = 0U;
uint8_t deepSleepMinutes = 0U;

static inline uint8_t isSafeToSleep(void)
{
  uint8_t safe = FALSE;
  
  if (FALSE == heatingOn)
  {
    safe = TRUE;
  }

  return safe;
}

// Digital clock display of the time
void timedActions()
{
  currentDate = String(day()) + "." + month() + "." + year();
  currentTime = String(hour()) + ":" + minute() + ":" + second();
  
  // Send time to the App
  Blynk.virtualWrite(V1, currentTime);
  
  // Send date to the App
  Blynk.virtualWrite(V2, currentDate);
  
  // Resync the current deep sleep values
  Blynk.virtualWrite(V5, deepSleepSeconds);
  Blynk.virtualWrite(V6, deepSleepMinutes);

  /* Check temperature setting */
  if ((temperatureSetting >= (currentTemperature + TEMP_DOWN_DIFF_LIMIT)) && 
      (FALSE == heatingOn))
  {
    heatingOn = TRUE;
    led1.on();
    digitalWrite(TEMP_RELAY_CTRL_PIN, HIGH);
    if (TRUE == debugOn)
    {
      TEMP_DEBUG_PRINT_WITH_TIMESTAMP("Heating is ON.");
    }
  }
  else if ((currentTemperature >= (temperatureSetting + TEMP_UP_DIFF_LIMIT)) &&
           (TRUE == heatingOn))
  {
    heatingOn = FALSE;
    led1.off();
    digitalWrite(TEMP_RELAY_CTRL_PIN, LOW);
    if (TRUE == debugOn)
    {
      TEMP_DEBUG_PRINT_WITH_TIMESTAMP("Heating is OFF.");
    }
  }

  // If the heating is on, the chip cannot sleep, otherwise the switch contacts are interrupted
  if ((TRUE == isSafeToSleep()) && (0U != (deepSleepSeconds | deepSleepMinutes)))
  {
    uint32_t sleepTime = TEMP_CONV_S_IN_US(deepSleepSeconds + TEMP_CONV_MIN_IN_S(deepSleepMinutes));

    if (TRUE == debugOn)
    {
      TEMP_DEBUG_PRINT_WITH_TIMESTAMP("Going to sleep now. I will be back after (seconds): " + String(sleepTime / 1000000U));
    }
    ESP.deepSleep(sleepTime);
  }
}

BLYNK_CONNECTED() {
  // Synchronize time on connection
  rtc.begin();
  Blynk.syncAll();
}

BLYNK_READ(V4)
{
  currentTemperature = (ktc.readCelsius() * 100);  // to get rid of decimals
  if (TRUE == debugOn)
  {
    TEMP_DEBUG_PRINT_WITH_TIMESTAMP("Current temperature (deg. C * 100): " + String(currentTemperature));
  }
  Blynk.virtualWrite(V4, (currentTemperature / 100.00f));  // divided by 100 to restore decimals
}

BLYNK_WRITE(V9)
{
  debugOn = param.asInt();
}

BLYNK_WRITE(V10)
{
  temperatureSetting = round(param.asFloat() * 100);
  Blynk.virtualWrite(V11, (temperatureSetting / 100.00f));  // divided by 100 to restore decimals
}

BLYNK_WRITE(V5)
{
  deepSleepSeconds = param.asInt();

  /* ESP8266 can sleep a maximum of aprox. 71 minutes. Nevertheless, limit second parameter to 59 (because we can) */
  if (deepSleepSeconds > 59U)
  {
    deepSleepSeconds = 59U;
  }
  
  Blynk.virtualWrite(V7, deepSleepSeconds);

  if (TRUE == debugOn)
  {
    TEMP_DEBUG_PRINT_WITH_TIMESTAMP("Deep sleep has been modified to (MM:SS): " + String(deepSleepMinutes) + ":" + String(deepSleepSeconds));
  }
}

BLYNK_WRITE(V6)
{
  deepSleepMinutes = param.asInt();

  /* ESP8266 can sleep a maximum of aprox. 71 minutes */
  if (deepSleepMinutes > 69U)
  {
    deepSleepMinutes = 69U;
  }

  if (TRUE == debugOn)
  {
    TEMP_DEBUG_PRINT_WITH_TIMESTAMP("Deep sleep has been modified to (MM:SS): " + String(deepSleepMinutes) + ":" + String(deepSleepSeconds));
  }
}

void setup()
{
    /* Pin that controls the relay */
    pinMode(TEMP_RELAY_CTRL_PIN, OUTPUT);
  
    // Debug console
    Serial.begin(9600);

    Blynk.begin(auth, ssid, pass);
    
    // Other Time library functions can be used, like:
    //   timeStatus(), setSyncInterval(interval)...
    // Read more: http://www.pjrc.com/teensy/td_libs_Time.html
  
    setSyncInterval(60 * 60); // Sync interval in seconds (60 minutes)
  
    // Display digital clock every 10 seconds
    timer.setInterval(TEMP_TIMED_ACTIONS_PERIOD_SECONDS * 1000U, timedActions);
}

void loop()
{
    Blynk.run();
    timer.run();
}


